﻿using System;
using NUnit.Framework;
using SupermarketBL;
using Moq;
using DataContract;
using System.Collections.Generic;

namespace Supermarket.Test
{
    [TestFixture]
    public class CartTest
    {
        Mock<Store> storeMock;

        /// <summary>
        /// Setup the store Mock
        /// </summary>
        [SetUp]
        public void Setup()
        {
            //Created a new product list
            Product p1 = new Product("cartTest1", 50);
            Product p2 = new Product("cartTest2", 100);
            List<Product> products = new List<Product>();
            products.Add(p1);
            products.Add(p2);

            //Mocks a store with the new product list
            storeMock = new Mock<Store>();
            storeMock.Setup(s => s.GetProducts()).Returns(products);
        }

        /// <summary>
        /// Tests initiating a new cart and adding products to it
        /// </summary>
        [Test]
        public void TestInitCart()
        {
            Cart testCart = new Cart(storeMock.Object);
            AddCommand addCommand = new AddCommand(testCart, "cartTest1");
            addCommand.Execute();
            Assert.IsNotNull(testCart.selectedProducts);
        }

        /// <summary>
        /// Tests adding a non-existing product
        /// </summary>
        [Test]
        public void TestAddingNonExistingProduct()
        {
            Cart testCart = new Cart(storeMock.Object);
            AddCommand addCommand = new AddCommand(testCart, "nonExistingProduct");
            Assert.Throws<Exception>(addCommand.Execute);
        }

        /// <summary>
        /// Tests executing undo twice
        /// </summary>
        [Test]
        public void TestUndoTwice()
        {
            Cart testCart = new Cart(storeMock.Object);
            AddCommand addCommand = new AddCommand(testCart, "cartTest1");
            addCommand.Execute();
            UndoCommand undoCommand = new UndoCommand(testCart);
            undoCommand.Execute();
            Assert.DoesNotThrow(undoCommand.Execute);
            Assert.AreEqual(testCart.selectedProducts.Count, 0);
        }
    }
}
