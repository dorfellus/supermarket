﻿using System;
using DataContract;
using NUnit.Framework;

namespace Supermarket.Test
{
    [TestFixture]
    public class ProductTest
    {
        /// <summary>
        /// Tests creating a product instance
        /// </summary>
        [Test]
        public void TestProductInit()
        {
            string productName = "sampleName";
            double productPrice = 20;

            Product newProduct1 = new Product(productName, productPrice);
            //Checks that the name and price of the product has been set
            Assert.AreEqual(newProduct1.Name, productName);
            Assert.AreEqual(newProduct1.Price, productPrice);

            //Checks that the name and price of the product has been set
            Product newProduct2 = new Product(productName, productPrice, new Guid());
            Assert.AreEqual(newProduct2.Name, newProduct1.Name);
            Assert.AreEqual(newProduct2.Price, newProduct1.Price);

            //Checks that the two products created have a different SKU
            Assert.AreNotEqual(newProduct1.SKU, newProduct2.SKU);
        }

        /// <summary>
        /// Tests creating a product instance with a negative price
        /// </summary>
        [Test]
        public void TestProductInitNegativePrice()
        {
            Assert.Throws<Exception>(() => new Product("testProduct", -20));
        }
    }
}
