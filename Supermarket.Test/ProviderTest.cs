﻿using System;
using DataContract;
using NUnit.Framework;
using ProductsProvider.Providers;

namespace Supermarket.Test
{
    [TestFixture ("JsonProvider")]
    [TestFixture ("SQLProvider")]
    public class ProviderTest
    {
        IProductsProvider provider;
        /// <summary>
        /// Constructor creates an instance of the current provider for each TestFixture
        /// </summary>
        /// <param name="providerName"></param>
        public ProviderTest(string providerName)
        {
            string formattedProviderName = "ProductsProvider.Providers." + providerName + ", ProductsProvider";
            provider = Activator.CreateInstance(Type.GetType(formattedProviderName)) as IProductsProvider;
        }

        /// <summary>
        /// Tests the loading of the products from the db
        /// </summary>
        [Test]
        public void TestProductsInit()
        {
            Assert.IsNotNull(provider.Products);
        }

        /// <summary>
        /// Tests adding a new product
        /// </summary>
        [Test]
        public void TestAddProductMethod()
        {
            Product testProduct = new Product("regularTestItem", 10);
            provider.AddProduct(testProduct);
            Assert.That(provider.Products.Contains(testProduct));
        }

        /// <summary>
        /// Tests updating product when adding a product with the same SKU twice
        /// </summary>
        [Test]
        public void TestAddingSameSKU()
        {
            Guid sku = new Guid("7E0837F6-1A15-47EE-9DE0-2BDE66A0271C");

            Product testProduct = new Product("sameGuidItem", 20, sku);
            provider.AddProduct(testProduct);
            testProduct = new Product("newSameGuidItem", 40, sku);
            provider.AddProduct(testProduct);

            //Checks if the products details has been updated
            Assert.That(provider.Products.Contains(testProduct));
        }
    }
}
