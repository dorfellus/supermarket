﻿
using SupermarketBL;
using System;
using DataContract;

namespace Supermarket
{
    class Program
    {
        static void Main(string[] args)
        {
            Store myStore = new Store();

            #region Test scenario #1 - Manager
            Console.WriteLine("Manager1 has entered the store");

            //Adds products to the db
            Product product1 = new Product("SanDisk USB Flash Drive", 20);
            if (myStore.AddProduct(product1))
            {
                Console.WriteLine("A new item has been added to the store: "
                    + product1.Name + ", Price: " + product1.Price + ", SKU: " + product1.SKU);
            }
            else
            {
                Console.WriteLine("Product " + product1.Name + " failed to load");
            }


            Product product2 = new Product("Apple iPad", 100);
            if (myStore.AddProduct(product2))
            {
                Console.WriteLine("A new item has been added to the store: "
                + product2.Name + ", Price: " + product2.Price + ", SKU: " + product2.SKU);
            }
            else
            {
                Console.WriteLine("Product " + product2.Name + " failed to load");
            }

            Console.WriteLine();
            #endregion

            #region Test scenario #2 - Customer
            Cart myCart = new Cart(myStore);
            Console.WriteLine("Customer1 has entered the store");

            //Adds items to the customer's cart
            string currItemName = "Sony headspeakers";
            AddCommand addCommand1 = new AddCommand(myCart, currItemName);
            addCommand1.Execute();
            Console.WriteLine("Item added: " + currItemName);

            currItemName = "Dell wireless keyboard";
            AddCommand addCommand2 = new AddCommand(myCart, currItemName);
            addCommand2.Execute();
            Console.WriteLine("Item added: " + currItemName);

            currItemName = "Dell wireless keyboard";
            AddCommand addCommand3 = new AddCommand(myCart, currItemName);
            addCommand3.Execute();
            Console.WriteLine("Item added: " + currItemName);

            currItemName = "SanDisk USB Flash Drive";
            AddCommand addCommand4 = new AddCommand(myCart, currItemName);
            addCommand4.Execute();
            Console.WriteLine("Item added: " + currItemName);

            UndoCommand undoCommand1 = new UndoCommand(myCart);
            undoCommand1.Execute();
            Console.WriteLine("Item Deleted: " + currItemName);

            //Checkout
            Console.WriteLine("Customer has checked out. Cart's total price: " + myCart.Checkout().ToString());

            //Clear items from the customer's cart
            myCart.ClearAllItems();
            if( myCart.selectedProducts.Count == 0)
            {
                Console.WriteLine("Cart is now empty");
            }

            Console.WriteLine();
            #endregion

            Console.ReadLine();




        }
    }
}
