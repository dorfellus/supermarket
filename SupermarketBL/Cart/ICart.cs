﻿using DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupermarketBL
{
    public interface ICart
    {
        void SetAction(ActionList action);
        void ExecuteAction(string name = "");
    }
}
