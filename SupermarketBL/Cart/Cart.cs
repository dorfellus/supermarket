﻿using System;
using System.Collections.Generic;
using DataContract;

namespace SupermarketBL
{
    public class Cart : ICart
    {
        public ActionList currAction;
        Store currStore;
        public List<Product> selectedProducts = new List<Product>();
        Product lastProductAdded;

        public Cart(Store store)
        {
            currStore = store;
        }

        void ICart.SetAction(ActionList action)
        {
            currAction = action;
        }

        void ICart.ExecuteAction(string name)
        {
            if (currAction == ActionList.AddProducts)
            {
                addproduct(name);
            }
            else if (currAction == ActionList.Undo)
            {
                Undo();
            }
        }

        private void addproduct(string name)
        {
            //Finds the product in the store product list and adds it to the user's cart
            int productIndex = currStore.GetProducts().FindIndex(product => product.Name.Equals(name, StringComparison.Ordinal));
            if(productIndex == -1)
            {
                throw new Exception("Item isn't available in store");
            }
            lastProductAdded = currStore.GetProducts()[productIndex];
            selectedProducts.Add(lastProductAdded);

        }

        private void Undo()
        {
            if (selectedProducts.Contains(lastProductAdded))
            {
                //Removes the last product added from the cart
                selectedProducts.Remove(lastProductAdded);
            }

        }

        public void ClearAllItems()
        {
            selectedProducts = new List<Product>();
        }

        public double Checkout()
        {
            //Calculates the cart's total price
            double totalPrice = 0;
            foreach (Product p in selectedProducts)
            {
                totalPrice += p.Price;
            }
            return totalPrice;
        }


    }
}
