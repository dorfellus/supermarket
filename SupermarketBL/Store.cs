﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ProductsProvider;
using ProductsProvider.Providers;
using System.Reflection;
using DataContract;

namespace SupermarketBL
{
    public class Store
    {
        IProductsProvider provider;

        public Store()
        {
            //Gets the selected db provider name from configuration
            String providerName = ConfigurationSettings.AppSettings.Get("currentProvider");
            if ((providerName != "JsonProvider") && (providerName != "SQLProvider"))
            {
                throw new Exception("Provider type is not supported");
            }
            string formattedProviderName = "ProductsProvider.Providers." + providerName + ", ProductsProvider";

            //Creates an instance of selected provider
            provider = Activator.CreateInstance(Type.GetType(formattedProviderName)) as IProductsProvider;
        }

        public virtual List<Product> GetProducts()
        {
            return provider.Products;
        }

        public bool AddProduct(Product currProduct)
        {
            return provider.AddProduct(currProduct);
        }
    }

}
