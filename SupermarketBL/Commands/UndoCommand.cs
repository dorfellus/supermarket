﻿using DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupermarketBL
{
    public class UndoCommand : ICommand
    {

        public UndoCommand(ICart cart) : base(cart){ }

        public override void Execute()
        {
            cart.SetAction(ActionList.Undo);
            cart.ExecuteAction();
        }
    }
}
