﻿using DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupermarketBL
{
    public class AddCommand : ICommand
    {
        string currName { get; set; }

        public AddCommand(ICart cart, string name) : base(cart)
        {
            currName = name;
        }

        public override void Execute()
        {
            cart.SetAction(ActionList.AddProducts);
            cart.ExecuteAction(currName);
        }
    }
}
