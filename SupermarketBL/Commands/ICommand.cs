﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupermarketBL
{
    public abstract class ICommand
    {
        public ICart cart = null;

        public ICommand(ICart currCart)
        {
            cart = currCart;
        }

        public abstract void Execute();
    }

}
