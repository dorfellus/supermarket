﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DataContract
{
    public class Product
    {
        public Product(string name, double price)
        {
            this.Name = name;
            if(price <= 0)
            {
                throw new Exception("Price must be a positive number");
            }
            this.Price = price;
            this.SKU = Guid.NewGuid();
        }

        [JsonConstructor]
        public Product(string name, double price, Guid sku)
        {
            this.Name = name;
            this.Price = price;
            this.SKU = sku;
        }

        public string Name { get; set; }
        public double Price { get; set; }
        public Guid SKU { get; set; }
    }
}
