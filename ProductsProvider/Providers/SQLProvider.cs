﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using DataContract;
using System.Data;

namespace ProductsProvider.Providers
{
    public class SQLProvider : IProductsProvider
    {
        string connString;
        string tableName;
        public override List<Product> Products { get; set; }

        public SQLProvider()
        {
            //Gets the connection string and table name from the configuration
            connString = ConfigurationSettings.AppSettings.Get("SQLProviderConnString");
            tableName = ConfigurationSettings.AppSettings.Get("SQLProviderTableName");
            initializeProducts();
        }

        private void initializeProducts()
        {
            Products = new List<Product>();

            //Load products from db
            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    //Opens connection with db
                    conn.ConnectionString = connString;
                    conn.Open();
                    SqlCommand command = new SqlCommand("SELECT * FROM " + tableName, conn);

                    //Load list o products from db
                    SqlDataReader productsReader = command.ExecuteReader();
                    Product p;
                    while (productsReader.Read())
                    {
                        p = new Product(
                            productsReader["Name"].ToString(), 
                            Double.Parse(productsReader["Price"].ToString()),
                            Guid.Parse(productsReader["sku"].ToString()));
                        Products.Add(p);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override bool AddProduct(Product product)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                if (product.Price <= 0)
                {
                    throw new Exception("Product price must be a positive number.");
                }

                try
                {
                    //Adds the product to the product list
                    Products.Add(product);

                    //Opens connection with db
                    conn.ConnectionString = connString;
                    conn.Open();

                    //Insert new product to db
                    string sql = "IF EXISTS(SELECT * FROM " + tableName + " WHERE SKU = @SKU) " +
                        "UPDATE " + tableName + " SET Name = @Name, Price = @Price " +
                        "WHERE SKU = @SKU " +
                        "ELSE " +
                        "INSERT INTO " + tableName + " (Name, Price, SKU) VALUES (@Name, @Price, @SKU)";
                    SqlCommand command = new SqlCommand(sql, conn);

                    command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = product.Name;
                    command.Parameters.Add("@Price", SqlDbType.Money).Value = product.Price;
                    command.Parameters.Add("@SKU", SqlDbType.UniqueIdentifier).Value = new System.Data.SqlTypes.SqlGuid(product.SKU);
                    command.CommandType = CommandType.Text;

                    command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    //Removes the products from the list since it has failed to upload to the db
                    Products.Remove(product);
                    return false;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
