﻿using DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsProvider.Providers
{
    public abstract class IProductsProvider
    {
        public abstract List<Product> Products { get; set; }
        public abstract bool AddProduct(Product product);
    }
}
