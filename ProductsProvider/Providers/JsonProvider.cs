﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Configuration;
using DataContract;
using System.Reflection;

namespace ProductsProvider.Providers
{
    public class JsonProvider : IProductsProvider
    {
        String fileLocation;
        public override List<Product> Products { get; set; }

        public JsonProvider()
        {
            //Gets the file location from configuration
            fileLocation = ConfigurationSettings.AppSettings.Get("JsonProviderFileLocation");
            initializeProducts();
        }

        private void initializeProducts()
        {
            Products = new List<Product>();

            //Load products from json file
            try
            {
                using (StreamReader r = new StreamReader(fileLocation))
                {
                    string json = r.ReadToEnd();
                    Products = JsonConvert.DeserializeObject<List<Product>>(json);
                }
            }
            catch(FileNotFoundException e)
            {
                throw new Exception("Json file path is not valid. Please check configuration");
            }
        }
        public override bool AddProduct(Product product)
        {
            //Validates price
            if (product.Price <= 0)
            {
                throw new Exception("Product price must be a positive number.");
            }

            //Checks if the SKU already exists in the list o products
            int productIndex = Products.FindIndex(p => p.SKU.Equals(product.SKU));
            if (productIndex != -1)
            {
                //If the SKU already exists, this would update the products details
                Products[productIndex] = product;
            }
            else
            {
                //Adds the product to the product list
                Products.Add(product);
            }

            try
            {
                //Adds the new products to the json file
                using (StreamWriter file = File.CreateText(fileLocation))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, Products);
                }
                return true;
            }
            catch
            {
                //Removes the products from the list since it has failed to upload to the db
                Products.Remove(product);
                return false;
            }
                

        }
    }
}
